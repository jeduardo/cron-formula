{%- if grains['os_family'] == 'Debian' %}
cron:
  pkg:
    - installed
  service:
    - running
{%- endif %}

{%- if grains['os_family'] == 'RedHat' %}
cronie:
  pkg:
    - installed

crond:
  service:
    - running
{%- endif %}
